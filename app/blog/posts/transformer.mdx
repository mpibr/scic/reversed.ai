---
title: 'Attention is all you need'
publishedAt: '2024-04-19'
summary: 'In our pilot meeting, we are reviewing the original introduction of the Transformer model.'
author: 'Georgi Tushev'
---

The seminal paper "Attention is All You Need" introduced the Transformer model, a groundbreaking architecture in the field of natural language processing. The key innovation of this model is its reliance solely on attention mechanisms, dispensing with the need for recurrent layers. The Transformer achieves high levels of performance and efficiency by using self-attention to compute representations of its input and output without requiring the sequential data processing typically found in recurrent neural networks. This allows for significantly improved training times due to parallelization, making it ideal for tasks that involve large datasets. The model's effectiveness and scalability have made it the foundation for many subsequent advances in machine learning, particularly in language understanding and translation tasks.


!["title"](/assets/posts/transformer/figure01_title.png)

###### cs > arXiv:[1706.03762](https://arxiv.org/abs/1706.03762), Jun 2017

### History of Natural Language Processing Tools

**Bag of Words (BoW)**

A text is represented as the bag (multi-set) of its words disregarding grammar and even word order but keeping multiplicity. The BoW model only accounts for the frequency of the words present in the text data.

```python
Sentence1 = "The cat sat on the mat."
Sentence2 = "The dog sat on the log."
BoW = ["the" : 2, "cat" : 1,
       "sat" : 2, "on" : 2,
       "mat" : 1, "dog" : 1, "log” : 1]
Vector1 = [2, 1, 1, 1, 1, 0, 0]
Vector2 = [2, 0, 1, 1, 0, 1, 1] 
```

**Word Embeddings**

Word representation that look at the semantic meanings of a word. Words with similar meanings are mapped to a similar point in the word embedding space.

!["word embeddings"](/assets/posts/transformer/figure02_wordembeddings.png)

###### Figure. Word embeddings, words with similar meaning will form clusters in an abstract laten space.

**Recurrent Neural Networks (RNNs)**

A class of neural networks that are powerful for modeling sequence data such as text. RNNs process inputs sequentially, maintaining an internal state from one timestep to the next to capture temporal dependencies within the input data.

**Long Short-Term Memory (LSTM)**

An advanced type of RNN specifically designed to avoid the long-term dependency problem. They are capable of learning long-term dependencies (context) in sequence data.

**Bi-directional LSTM**

An extension of traditional LSTMs that improve the model's context understanding. They process data in both forward and backward directions, which provides additional context to the network.

!["RNNs"](/assets/posts/transformer/figure03_RNNs.png)

###### Figure. In generic RNNs, each step of the model transfers minimal context to subsequent iterations.

**Attention based model**

Attention mechanisms allow models to focus on different parts of the input sequence when performing tasks, enhancing the ability of the model to capture important relationships in data.

!["Attention Graph"](/assets/posts/transformer/figure04_AttentionModels.png)

###### Figure. Attention-based models can be conceptualized as graphs, where the nodes represent tokens and the edges signify the attention scores between tokens within a given input sequence.

### Innovations by the transformer model

**Encoder Decoder Architecture**

The encoder processes the input data into a context-rich representation, which the decoder then uses to generate the output sequence step-by-step.

!["Encoder-Decoder"](/assets/posts/transformer/figure05_EncoderDecoder.png)

###### Figure. Abstract Representation of Encoder-Decoder Architecture. This diagram illustrates the fundamental components of the encoder-decoder model. The encoder (left) processes the input sequence, transforming it into a compact, context-rich representation. This encoded state is then passed to the decoder (right), which sequentially generates the output sequence. Each component consists of layers that capture and transform information, facilitating complex tasks such as language translation.

**Positional Encodings**

Vectors to provide information about the relative or absolute positions of tokens in a sequence. The aggregate of various sine and cosine waves across different frequencies, aligned with the range of tokens, establishes a distinct numerical representation for each token position.

!["Positional Encodings"](/assets/posts/transformer/figure06_PositionalEncodings.png)

###### Figure. In the original paper, the authors employed a mix of 1,000 sine and cosine waves, each at varying frequencies.

**Self-attention**

A mechanism within neural networks that allows models to weigh the importance of different parts of the input data relative to each other for better context understanding and representation.

*Query (Q)* :
Represents the element for which the attention needs to be calculated. It is the word (in transformed embedding space) that is currently being focused on by the model.

*Key (K)* :
All the elements in the sequence. Keys will be compared against the query to compute attention score. Essentially, keys provide a way to access the values.

*Value (V)* :
Representations of the words in the input sequence. Once the attention scores are computed based on Q and K, these scores weigh the values to result in the output of the self-attention layer.

!["Self Attention"](/assets/posts/transformer/figure07_SelfAttention.png)

###### Figure 2 in the original paper: (left) Scaled Dot-Product Attention. (right) Multi-Head Attention consists of several attention layers running in parallel.

Combinations of "Self Attention" modules in a stack is called Multi-Head Attention. Each module in the stack can be efficiently parallelised.

### The Transformer

**Encoder**

The input consists of a combination of word embeddings, which convert the input text sequence into numerical values, and positional encodings that establish the positional relationships of tokens in sequence. The self-attention layer then maps out the relationships between words. Following this, a feed-forward network modifies the output's representation space from the self-attention layer. Additionally, residual connections between layers are notable, as they help mitigate the issue of vanishing gradients during training.

**Decoder**

The input to the decoder undergoes the same preprocessing as the encoder, but it processes one token at a time, generated from the previous inference or iteration. The initial self-attention layer in the decoder is masked, which means that in the attention mechanism, all connections from unseen tokens are excluded. This design ensures that the model only focuses on the tokens it has already processed. A key distinction from the encoder is the presence of the encoder-decoder attention layer. This layer takes inputs from the encoder and uses them to focus on pertinent parts of the input sequence by leveraging its current state to query the contextually rich output produced by the encoder. While the input to the encoder remains constant during the generation process, the query (Q), key (K), and value (V) weights are dynamically updated. The final feed-forward network may be adjusted to add or remove any potential biases in the results. The output is generated sequentially, token by token.

!["The Transformer"](/assets/posts/transformer/figure08_TheTransformer.png)

###### Figure 1 in the original paper: The Transformer - model architecture.

### Summary

**Parallel Processing**

Transformers enable the parallel processing of sequence data, significantly improving training speeds over previous models like RNNs and LSTMs, which require sequential processing.

**Self-Attention Mechanism**

The self-attention mechanism allows Transformers to weigh the importance of each part of the input data, enhancing their ability to understand complex dependencies and relationships within the data.

**Scalability and Versatility**

Due to their architecture, Transformers are highly scalable and have shown remarkable versatility, excelling in a range of tasks from natural language processing to image recognition and beyond.