import { baseUrl } from 'app/sitemap';
import Image from 'next/image';

function ArrowIcon() {
  return (
    <svg
      width="12"
      height="12"
      viewBox="0 0 12 12"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M2.07102 11.3494L0.963068 10.2415L9.2017 1.98864H2.83807L2.85227 0.454545H11.8438V9.46023H10.2955L10.3097 3.09659L2.07102 11.3494Z"
        fill="currentColor"
      />
    </svg>
  )
}

export default function Footer() {
  return (
    <footer className="mb-16">
      <ul className="font-sm mt-8 flex flex-col space-x-0 space-y-2 text-neutral-600 md:flex-row md:space-x-4 md:space-y-0 dark:text-neutral-300">
        <li>
          <a
            className="flex items-center transition-all hover:text-neutral-800 dark:hover:text-neutral-100"
            rel="noopener noreferrer"
            target="_blank"
            href="https://gitlab.mpcdf.mpg.de/mpibr"
          >
            <ArrowIcon />
            <p className="ml-1 h-7">gitlab</p>
          </a>
        </li>
        <li>
          <a
            className="flex items-center ml-3 transition-all hover:text-neutral-800 dark:hover:text-neutral-100"
            rel="noopener noreferrer"
            target="_blank"
            href="https://gitlab.mpcdf.mpg.de/mpibr/scic/reversed.ai"
          >
            <ArrowIcon />
            <p className="ml-1 h-7">source</p>
          </a>
        </li>
        <li>
          <a
            className="flex items-center ml-3 transition-all hover:text-neutral-800 dark:hover:text-neutral-100"
            rel="noopener noreferrer"
            target="_blank"
            href="https://listserv.gwdg.de/mailman/listinfo/reversed.ai"
          >
            <ArrowIcon />
            <p className="ml-1 h-7">subscribe</p>
          </a>
        </li>
      </ul>
      <p className="mt-8 text-sm text-neutral-600 dark:text-neutral-400">
      powered by Next.js, {new Date().getFullYear()}
      </p>  
      <div className="mt-8 flex items-center text-neutral-600 dark:text-neutral-300">
        <img src={`${process.env.NEXT_PUBLIC_BASE_PATH}/assets/logo/logo_mpibr_dark.png`} alt="logo_mpibr" className="h-8 lg:h-10 mb-2" />
        <img src={`${process.env.NEXT_PUBLIC_BASE_PATH}/assets/logo/logo_scic_dark.png`} alt="logo_scic" className="ml-6 h-8 lg:h-10 mb-2" />
      </div>
    </footer>
  )
}
