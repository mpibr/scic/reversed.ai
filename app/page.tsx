import { BlogPosts } from 'app/components/posts'

export default function Page() {
  return (
    <section>
      <h1 className="mb-8 text-2xl font-semibold tracking-tighter">
        reversed.ai
      </h1>
      <p className="mb-4 text-justify">
        {`We are a group of AI enthusiasts at Max Planck Institute for Brain
          Research. We've launched this blog driven
          by the vision and dedication to explore all the latest developments in
          AI and machine learning, and their applications in cutting-edge research.`}
      </p>
      <div className="my-8">
        <BlogPosts />
      </div>
    </section>
  )
}
