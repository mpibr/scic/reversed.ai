/** @type {import('next').NextConfig} */

const isProd = process.env.NODE_ENV === 'production';
const basePathProd = isProd ? '/scic/reversed.ai' : undefined;
process.env.NEXT_PUBLIC_BASE_PATH = basePathProd;

const nextConfig = {
    output: 'export',
    basePath: basePathProd,
    assetPrefix: basePathProd
};
  
module.exports = nextConfig;
